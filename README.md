# Commit User Test

Testing a reported issue where a user suggests a change to an MR and the change is accepted. The user is listed as the author of the commit but is unable to control the commit message. Concerned about a privacy/security issue